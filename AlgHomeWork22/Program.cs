﻿public class Program
{
    public static void Main()
    {
        // Задача про горох 
        Console.WriteLine("Задача про горох");
        Console.WriteLine("Введите начальные данные в виде a/b+c/d");
        string input = Console.ReadLine();
        string[] parts = input.Split('+');

        string part1 = parts[0];
        string part2 = parts[1];

        string[] fraction1 = part1.Split('/');
        string[] fraction2 = part2.Split('/');

        int a = int.Parse(fraction1[0]);
        int b = int.Parse(fraction1[1]);
        int c = int.Parse(fraction2[0]);
        int d = int.Parse(fraction2[1]);

        int nok = NOK(b, d);

        int num1 = a * (nok / b);
        int num2 = c * (nok / d);

        int nod = NOD(num1 + num2, nok);

        Console.WriteLine($"{(num1 + num2) / nod}/{nok / nod}");
        Console.WriteLine("Нажмите, чтобы продолжить");
        Console.ReadKey();

        Console.WriteLine();
        

        // Задача про ёлочку
        Console.WriteLine("Задача про ёлочку");
        Console.WriteLine("Введите высоту елочки:");
        int height = int.Parse(Console.ReadLine());
        int i;

        List<int[]> tree = new List<int[]>(height);
        for (i = 0; i < height; i++)
        {
            Console.WriteLine($"Линия {i}");
            string[] temp = Console.ReadLine().Trim().Split(' ');
            int[] row = Array.ConvertAll(temp, int.Parse);
            tree.Add(row);
        }

        int maxColumns = tree.Max(t => t.Length);
        int[,] dp = new int[height, maxColumns];
        for (i = 0; i < height; i++)
        {
            for (int j = 0; j < tree[i].Length; j++)
            {
                if (i == 0 && j == 0)
                {
                    dp[i, j] = tree[i][j];
                }
                else if (i == 0)
                {
                    dp[i, j] = Math.Max(dp[i, j - 1], tree[i][j]);
                }
                else if (j == 0)
                {
                    dp[i, j] = Math.Max(dp[i - 1, j], tree[i][j]);
                }
                else
                {
                    dp[i, j] = Math.Max(Math.Max(dp[i - 1, j], dp[i, j - 1]), tree[i][j]);
                }
            }
        }

        int maxSum = 0;
        for (i = 0; i < height; i++)
        {
            maxSum += dp[i, i];
        }

        Console.WriteLine($"Максимальная сумма гирлянды - { maxSum }");
        Console.WriteLine();



        // Задача 5-8
        Console.WriteLine("Задача 5-8");
        Console.WriteLine("Введите N");
        string inputN = Console.ReadLine();
        int n = Convert.ToInt32(inputN);
        int[, ,] array;

        long x1 = 2;
        long x2 = 4;
        i = 3;
        while (i <= n) 
        {
            x2 += x1;
            x1 = x2 - x1;
            i++;
        }
        var res = n == 1 ? x1 : x2;
        Console.WriteLine($"Результат для {n} - {res}");
        Console.WriteLine();

        

        // Острова
        Console.WriteLine("Задача про острова");
        Console.WriteLine("Введите N");
        string inputIslN = Console.ReadLine();
        int islN = Convert.ToInt32(inputIslN);
        bool[,] visited; // Матрица для отслеживания посещенных ячеек

        Console.WriteLine("Введите матрицу для \"островов\"");
        int[,] matrix = new int[islN, islN];
        for (i = 0; i < islN; i++)
        {
            Console.WriteLine($"Линия - {i}");
            string[] line = Console.ReadLine().Split(' ');
            for (int j = 0; j < islN; j++)
            {
                matrix[i, j] = Convert.ToInt32(line[j]);
            }
        }

        var count = 0;
        CountIslands();
        Console.WriteLine($"Количество островов - {count}");

        Console.ReadKey();

        void DFS(int i, int j)
        {
            if (i < 0 || 
                i >= islN || 
                j < 0 || 
                j >= islN || 
                visited[i, j] || 
                matrix[i, j] == 0)
            {
                return;
            }

            visited[i, j] = true;

            DFS(i - 1, j);
            DFS(i + 1, j);
            DFS(i, j - 1);
            DFS(i, j + 1);
        }

        int CountIslands()
        {
            visited = new bool[islN, islN];

            for (i = 0; i < islN; i++)
            {
                for (int j = 0; j < islN; j++)
                {
                    if (!visited[i, j] && matrix[i, j] == 1)
                    {
                        DFS(i, j);
                        count++;
                    }
                }
            }

            return count;
        }
    }

    private static int NOK(int a, int b)
    {
        return a * b / NOD(a, b);
    }

    private static int NOD(int a, int b)
    {
        while (b != 0)
        {
            int temp = b;
            b = a % b;
            a = temp;
        }
        return a;
    }
}
